import { parse } from '@utils/TaskEither.ts';
import { z } from 'zod';

export const meteoChartUrl = () =>
  parse(z.string().url())(process.env.LABSKA_BOUDA_STATION_METEOCHART_URL);
