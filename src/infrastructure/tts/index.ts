import { AppError } from '@core/model/error.ts';
import { tryExecute } from '@utils/TaskEither.ts';
import { write } from 'bun';
import { pipe } from 'fp-ts/function';
import { flatMap, map, TaskEither } from 'fp-ts/TaskEither';
import OpenAI from 'openai';
import { Readable } from 'stream';

const Speaker = require('audio-speaker/stream');
export const saveTTSToFile =
  (path: string) =>
  (text: string): TaskEither<AppError, string> => {
    const fileName = path + '/' + new Date().toISOString() + '.mp3';
    return pipe(
      tryExecute(
        () =>
          new OpenAI().audio.speech.create({
            model: 'tts-1-hd',
            voice: 'onyx',
            input: text,
            response_format: 'mp3',
            speed: 1.05,
          }),
        'TTS with OpenAI',
      ),
      flatMap(response => tryExecute(() => write(fileName, response), 'Write TTS to file')),
      map(() => fileName),
    );
  };

export const playTTS = (text: string): TaskEither<AppError, any> =>
  pipe(
    tryExecute(
      () =>
        new OpenAI().audio.speech.create({
          model: 'tts-1',
          voice: 'onyx',
          input: text,
          response_format: 'mp3',
          speed: 1.05,
        }),
      'TTS with OpenAI',
    ),
    flatMap(response =>
      tryExecute(() => response.arrayBuffer(), 'Convert TTS result to ArrayBuffer'),
    ),
    map(arrayBuffer => Buffer.from(arrayBuffer)),
    map(buffer =>
      Readable.from(buffer).pipe(
        Speaker({
          channels: 2, // 2 channels
          bitDepth: 16, // 16-bit samples
          sampleRate: 44100,
        }),
      ),
    ),
  );
