import { AppError } from '@core/model/error.ts';
import { answerAsMeteorologist } from '@core/uc/labskaBoudaWeatherStationUC.ts';
import { saveTTSToFile } from '@infrastructure/tts';
import { tap, tryExecute } from '@utils/TaskEither.ts';
import { match } from 'fp-ts/Either';
import { pipe } from 'fp-ts/function';
import { flatMap, map, of, TaskEither } from 'fp-ts/TaskEither';
import { get, start, stop } from 'prompt';

const conversationLoop = (): TaskEither<AppError, void> =>
  pipe(
    tryExecute(
      () => get([{ name: 'question', description: 'Your question' }]),
      'Get user question',
    ),
    flatMap(({ question }) => answerAsMeteorologist(question as string)),
    tap(answer => console.log(answer)),
    flatMap(saveTTSToFile(import.meta.dir)),
    flatMap(() =>
      tryExecute(() => get([{ description: 'Continue? (y/n)', name: 'more' }]), 'Continue'),
    ),
    map(({ more }) => (more as string) === 'y'),
    flatMap(more => (more ? conversationLoop() : of(undefined))),
  );

start({ message: 'Meteo' });
console.log(`Hello I am your meteorologist from Labska Bouda Weather Station. You can ask me about weather conditions for last 48h.
How can I help You?`);
pipe(
  await conversationLoop()(),
  match(
    e => console.error(e),
    r => undefined,
  ),
);
stop();
