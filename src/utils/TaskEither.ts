import { AppError } from '@core/model/error.ts';
import * as E from 'fp-ts/Either';
import { pipe } from 'fp-ts/function';
import * as TE from 'fp-ts/TaskEither';
import { map, mapLeft, TaskEither, tryCatch } from 'fp-ts/TaskEither';
import { ZodType } from 'zod';

export const tryExecute = <E, T>(lazy: () => Promise<T>, desc: string): TaskEither<AppError, T> =>
  tryCatch(lazy, err => AppError.fromError(desc, err));

export const tap =
  <E, T>(f: (t: T) => void) =>
  (te: TaskEither<E, T>): TaskEither<E, T> =>
    pipe(
      te,
      map(t => {
        f(t);
        return t;
      }),
    );

export const tapLeft =
  <E, T>(f: (t: E) => void) =>
  (te: TaskEither<E, T>): TaskEither<E, T> =>
    pipe(
      te,
      mapLeft(e => {
        f(e);
        return e;
      }),
    );

export const parse =
  <T>(schema: ZodType<T>) =>
  (value: unknown): TE.TaskEither<AppError, T> =>
    pipe(
      E.tryCatch(
        () => schema.parse(value),
        e => AppError.fromError('Parse error', e),
      ),
      TE.fromEither,
    );
