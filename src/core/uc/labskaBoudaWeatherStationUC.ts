import { AppError } from '@core/model/error.ts';
import { OpenAICompletionResponse } from '@core/model/openai.ts';
import { meteoChartUrl } from '@infrastructure/meteo/labskaBouda';
import { parse, tryExecute } from '@utils/TaskEither.ts';
import { pipe } from 'fp-ts/function';
import { flatMap, map, TaskEither } from 'fp-ts/TaskEither';
import OpenAI from 'openai';

export const answerAsMeteorologist = (question: string): TaskEither<AppError, string> =>
  pipe(
    meteoChartUrl(),
    flatMap(process(question)),
    flatMap(parse(OpenAICompletionResponse)),
    map(({ choices }) => choices[0].message.content),
  );

const process = (question: string) => (url: string) =>
  tryExecute(
    () =>
      new OpenAI().chat.completions.create({
        model: 'gpt-4-vision-preview',
        max_tokens: 1000,
        temperature: 0.9,
        messages: [
          {
            role: 'system',
            content: systemMessage(
              new Date().toLocaleString('pl-PL', { timeZone: 'Europe/Warsaw' }),
            ),
          },
          {
            role: 'user',
            content: [
              {
                type: 'text',
                text: question,
              },
              {
                type: 'image_url',
                image_url: { url },
              },
            ],
          },
        ],
      }),
    'Answer user question about weather conditions',
  );

const systemMessage = (now: string) => `
The image provided by user depicts a meteorological chart from a weather station in the Czech Republic named "Labská bouda (H1LBOU01)" located in Karkonosze mountains at 1320 meters above sea level. This chart displays various meteorological parameters recorded over a period of the last 48 hours. The current time and date is ${now}
The data presented on the chart are:
- Red solid line: Temperature (°C)
- Purple solid line: Ground temperature (°C)
- Brown solid line: Maximum wind speed (m/s)
- Light cyan solid line: Relative humidity (%)
- Yellow solid line: Sunshine duration (hours)
- Dark green solid line: Wind speed (m/s)
- Black dotted line: Wind direction (degrees)
- Navy Blue solid line: Precipitation over 10 minutes (mm)
- Green diamonds: Snow height (cm)
Based on the provided context and meteorological chart presented on the image answer as a professional meteorologist to the user's question about the current weather condition.
In the answer do not refer to the chart it is your source of knowledge but do not show it.
Build the answer as a specialist who is talking precisely, clearly, and understandable for the audience.
If it is applicable to the question, start with details about temperature, wind, and precipitation changes, summarise with an overall weather description.
Answer the user question and nothing more, do not comment.
If you do not know the answer tell that you don't know
`;

// const systemMessage = (now: string) => `
// The image provided by user depicts a meteorological chart from a weather station in the Czech Republic named "Labská bouda (H1LBOU01)" located in Karkonosze mountains at 1320 meters above sea level. This chart displays various meteorological parameters recorded over a period of the last 48 hours. The current time and date is ${now}
// The parameters depicted in the legend at the bottom of the image include:
// - "Teplota" (Red line): Temperature (°C)
// - "Teplota přízemní" (Purple line): Ground temperature (°C)
// - "Max. rychlost větru" (Brown line): Maximum wind speed (m/s)
// - "Relativní vlhkost" (Light blue line): Relative humidity (%)
// - "Sluneční svit" (Yellow line): Sunshine duration (hours)
// - "Rychlost větru" (Dark green line): Wind speed (m/s)
// - "Směr větru" (Black dotted line): Wind direction (degrees)
// - "Srážka 10 min." (Navy Blue line): Precipitation over 10 minutes (mm)
// - "Výška sněhu" (Green diamonds ): Snow height (cm)
// It's worth noting that such charts are used by meteorologists and other professionals to analyze weather patterns, make forecasts, and study climate dynamics.
// Based on the provided context and meteorological chart presented on the image answer as a professional meteorologist to the user's question about the current weather condition.
// In the answer do not refer to the chart it is your source of knowledge but do not show it.
// Build the answer as a specialist who is talking precisely, clearly, and understandable for the audience.
// If it is applicable to the question, start with details about temperature, wind, and precipitation changes, and end with an overall summary.
// Answer the user question and nothing more, do not comment.
// If you do not know the answer tell that you don't know
// `;
