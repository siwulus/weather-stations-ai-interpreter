import { z } from 'zod';

export const OpenAICompletionResponse = z.object({
  choices: z
    .array(
      z.object({
        message: z.object({
          content: z.string(),
        }),
      }),
    )
    .nonempty(),
});

export type OpenAICompletionResponse = z.infer<typeof OpenAICompletionResponse>;
