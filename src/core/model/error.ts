import { isNotNil } from 'ramda';

export class AppError extends Error {
  public readonly code: number;
  public readonly data: any;
  constructor(message: string, code: number, data: any) {
    super(message);
    this.code = code;
    this.data = data;
  }
  public static fromError = (message: string, err: any): AppError => {
    return new AppError(`${message}${isNotNil(err.message) ? ': ' + err.message : ''}`, 400, err);
  };
}